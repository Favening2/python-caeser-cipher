Dict = {
        "a" : 1,
        "b" : 2,
        "c" : 3,
        "d" : 4,
        "e" : 5,
        "f" : 6,
        "g" : 7,
        "h" : 8,
        "i" : 9,
        "j" : 10,
        "k" : 11,
        "l" : 12,
        "m" : 13,
        "n" : 14,
        "o" : 15,
        "p" : 16,
        "q" : 17,
        "r" : 18,
        "s" : 19,
        "t" : 20,
        "u" : 21,
        "v" : 22,
        "w" : 23,
        "x" : 24,
        "y" : 25,
        "z" : 26,
    }

DictNum = {
        1 : "a",
        2 : "b",
        3 : "c",
        4 : "d",
        5 : "e",
        6 : "f",
        7 : "g",
        8 : "h",
        9 : "i",
        10 : "j",
        11 : "k",
        12 : "l",
        13 : "m",
        14 : "n",
        15 : "o",
        16 : "p",
        17 : "q",
        18 : "r",
        19 : "s",
        20 : "t",
        21 : "u",
        22 : "v",
        23 : "w",
        24 : "x",
        25 : "y",
        26 : "z",
}

symbols = "!@#$%^&*(){}[]|:<?>,.;\//~`'-+_="

def number_Check(text):
    return any(i.isdigit() for i in text)

def encrypt(text, key):

        text = text.lower()

        final_encryption = ""

        for i in range(0, len(text)):

                if text[i] == " ":

                        final_encryption = final_encryption + " "
                        continue
                
                if number_Check(text[i]):
                        final_encryption = final_encryption + text[i]
                        continue

                if text[i] in symbols:
                        final_encryption = final_encryption + text[i]
                        continue

                #final_encryption = final_encryption + DictNum[Dict[text[i]] + int(key)]
                # This little line of code would be the same as line 92 but if i shirnk it down to the code above i will lose the overflow protection which is very important to this encryption so i will think of more ways to use the obove

                letternum = Dict[text[i]] + int(key)

                if letternum > 26:
                        letternum = letternum - 26

                final_encryption = final_encryption + DictNum[letternum]

        return final_encryption


def decrypt(text, key):

        text = text.lower()

        final_decryption = ""

        # Function for the AutoDecrypt

        if key == "a":

            for a in range(1, 26):

                for i in range(0, len(text)):

                    if text[i] == " ":
                        final_decryption = final_decryption + " "
                        continue

                    if number_Check(text[i]):
                        final_decryption = final_decryption + text[i]
                        continue
                    
                    if text[i] in symbols:
                        final_decryption = final_decryption + text[i]
                        continue

                    letternum = Dict[text[i]] - int(a-1)

                    if letternum <= 0:

                        letternum = letternum + 26
                        
                    final_decryption = final_decryption + DictNum[letternum]

                final_decryption = final_decryption + "\n" + "-"*50 + "\n" 

            return final_decryption

        for i in range(0, len(text)):

                if text[i] == " ":

                        final_decryption = final_decryption + " "
                        continue

                letternum = Dict[text[i]] - int(key)

                if letternum <= 0:

                        letternum = letternum + 26

                final_decryption = final_decryption + DictNum[letternum]

        return final_decryption

def main():

        print("-------------------------")
        print("|  Ceaser Cipher! v.04.2 |")
        print("-------------------------")

        # Version 4 can now handle symbols but the will not accounted in the cipher also there is still no error checking

        ans = input("Do you want to encrypt or decrypt? (e or d):")

        if ans == "e":
                text = input("Enter the text you want to encrypt:")
                key = input("Enter the key you want to encrypt it with (Max 39):")

                try:
                  print(encrypt(text,key))
                except KeyError as e:
                  print("Error:" + str(e) + " Tring not useing double quotation marks")

        elif ans == "d":

                text = input("Enter the text you want to decrypt:")
                key = input("Enter the key you want to encrypt it with (Max 26) of if you want to test every key enter (a):")

                try:
                  print(decrypt(text,key))
                except KeyError:
                  print("Error")

        else:
                print("Incorrect input")

while True:
  main()
