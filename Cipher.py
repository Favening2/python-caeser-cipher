Dict = {
        "a" : 1,
        "b" : 2,
        "c" : 3,
        "d" : 4,
        "e" : 5,
        "f" : 6,
        "g" : 7,
        "h" : 8,
        "i" : 9,
        "j" : 10,
        "k" : 11,
        "l" : 12,
        "m" : 13,
        "n" : 14,
        "o" : 15,
        "p" : 16,
        "q" : 17,
        "r" : 18,
        "s" : 19,
        "t" : 20,
        "u" : 21,
        "v" : 22,
        "w" : 23,
        "x" : 24,
        "y" : 25,
        "z" : 26,
    }

DictNum = {
        1 : "a",
        2 : "b",
        3 : "c",
        4 : "d",
        5 : "e",
        6 : "f",
        7 : "g",
        8 : "h",
        9 : "i",
        10 : "j",
        11 : "k",
        12 : "l",
        13 : "m",
        14 : "n",
        15 : "o",
        16 : "p",
        17 : "q",
        18 : "r",
        19 : "s",
        20 : "t",
        21 : "u",
        22 : "v",
        23 : "w",
        24 : "x",
        25 : "y",
        26 : "z",
}



def encrypt(text, key):

        text = text.lower()

        final_encryption = ""

        for i in range(0, len(text)):

                if text[i] == " ":

                        final_encryption = final_encryption + " "
                        continue

                letternum = Dict[text[i]] + int(key)

                if letternum > 26:
                        letternum = letternum - 26

                final_encryption = final_encryption + DictNum[letternum]

        return final_encryption


def decrypt(text, key):

        text = text.lower()
        
        final_decryption = ""

        for i in range(0, len(text)):

                if text[i] == " ":

                        final_decryption = final_decryption + " "
                        continue

                letternum = Dict[text[i]] - int(key)

                if letternum <= 0:

                        letternum = letternum + 26

                final_decryption = final_decryption + DictNum[letternum]

        return final_decryption

def __main__():

        print("-------------------------")
        print("|  Ceaser Cipher! v.02  |")
        print("-------------------------")

        ans = input("Do you want to encrypt or decrypt? (e or d):")

        if ans == "e":
                text = input("Enter the text you want to encrypt:")
                key = input("Enter the key you want to encrypt it with (Max 39):")
                print(encrypt(text,key))
        elif ans == "d":
                text = input("Enter the text you want to decrypt:")
                key = input("Enter the key you want to encrypt it with (Max 39):")
                print(decrypt(text,key))
        else:
                print("Incorrect input")

